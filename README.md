
<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [About Autonomous Tree](#about-autonomous-tree)
- [Objects and Artificial Sensors](#objects-and-artificial-sensors)
- [Control Box](#control-box)
- [Code](#code)
- [Mounting Structure and Signage](#mounting-structure-and-signage)
- [Testing and Setting Up](#testing-and-setting-up)
- [Next Steps](#next-steps)
- [License](#license)



<!-- END doctoc generated TOC please keep comment here to allow auto update -->

![alt text](https://gitlab.com/krzysztof_wronski/autonomous-tree/-/raw/master/images/challenge4-intro.png "Project Introduction")

<br>

## About Autonomous Tree

The Autonomous Tree is a result of my project Hypothetical Authorities, where people collaborated with me to explore and define potential new authorities that could address the largest challenges of our time, stimulate critical dialogues about the way authorities are designed today, and depict alternative possiblities for how things could be for audiences to consider. There are three conceptual parts to the Autonomus Tree installation. First, the theoretical possibiltiy that a selected tree could serve as a political representative, beaurocrat, or otherwise have power over humans. This is mostly achieved through storytelling and the narrative surrounding the installation. Second, physical symbolic artefacts and devices associated with human authority today installed on and around the tree. This was the focus of my Fab Academy Challenge 4 project and this repository houses all of the assets and files utilised to create these objects and the functionality they have. Finally, the last part of Autonomous Tree is a a digital conversation, created using a SaaS chat-bot creator called Landbot, which enables a dialogue and discourse between a human visiting the tree and the tree itself using the visitor's phone and a QR code which starts the conversation in the context of the tree.
<br>


![alt text](https://gitlab.com/krzysztof_wronski/autonomous-tree/-/raw/master/images/challenge4-2.jpg "Finished Installation")

<br>

## Objects and Artificial Sensors

I wanted to replicate the look and feel of certain industrial components utilised on autonomous vehicles and security aparatus today but avoid the expense and difficulity of actually acquiring these devices. Inspired by the fact that fake security cameras are a commonly installed psychological deterent to theft, I thought it would be interesting from an artistic perspective to replicate other devices in the same style including a marine radar, a lidar, a GNSS antenna, and an anemometer. My first step was to create original 3D models of these devices using visual references of their authentic counterparts. These were then 3D printed and assembled to appear realistic enough to be identifiable as the referenced devices. 
<br>

![alt text](https://gitlab.com/krzysztof_wronski/autonomous-tree/-/raw/master/images/challenge4-cad-models.png "Radar and GNSS Antenna CAD Models")
<br>

All of the objects had significant fabrication complexity despite being non-functional sensors. They are multi-part assemblies incorporating metal hardware to stay together. The lidar housing and radar are designed to use a small DC motor, in my case a 12V motor, so that they spin 200rpm and 15rpm respectively. The anemometer spins freely in the wind thanks to a roller-blade bearing (608zz). Credit to the design of the anemometer goes to Flash Gorshkov and can be found here: (https://www.thingiverse.com/thing:2523343).
<br>

![alt text](https://gitlab.com/krzysztof_wronski/autonomous-tree/-/raw/master/images/challenge4-cad-lidar.gif "Lidar CAD Model")
<br>

All models were printed consistently on Ender 3D printers configured in Cura's software with the following settings with one exception documented below this default list:

- Plate temperature: 215C
- Extruder temperature: 205C
- Plate adhesion: Skirt
- Infill density: 15%
- Layer height: 0,2
- Supporting material: Needed for antenna housing and lidar upper housing only with 10% density

As shown in the below images, the lidar housing required unique considerations when printing due to its complex shape. An algorithmic tree structure was generated as supporting material in the Cura software to reduce material and print time. 
<br>

![alt text](https://gitlab.com/krzysztof_wronski/autonomous-tree/-/raw/master/images/challenge4-stl.png "3D printing")
<br>

Finally, some of the additional objects for the installation were either hand-made or purchased as standalone. The rotating radar-like part was constructed from high density foam, cut to shape using 45 degree cuts with a ciricular saw on a sliding jig. A coating of wood glue, white acrylic paint, and vinyl lettering followed by an application of clear coat were used to create an authentic looking radar with Furuno branding. Three 3D printed parts, two end-caps and one bracket made the radar part more durable and enabled it to easily be taken on and off of the motor. The blinking hazard lamp and fake security camera were purchased and then modified to integrate into the control box circuitry.
<br>

![alt text](https://gitlab.com/krzysztof_wronski/autonomous-tree/-/raw/master/images/challenge4-radar.png "Radar top")

<br>

**Inventory for Objects:**

- Blue foam for radar top
- PLA filament
- 1x 608zz bearing for anemometer
- 1x DC 12 V Motor 200RPM
- 1x DC 12 V Motor 15RPM

## Control Box

In collaboration with Pietro Rustici (https://pietro_rustici.gitlab.io/mdef-website/), I worked to assemble a control box to actuate the motorised objects and blinking safety light. In addition, the box would handle regulating power to all the parts and feature a key operated switch and dot-matrix LED panel for message display and queue management functionality. These are all controlled with an ESP32 microcontroller. Finally, the circuit also incorporated a fail-safe emergency stop button for fun and aesthetic value. Even if the key is turned to the on position and power is available, the emergency stop button must be plugged in and in its inactive state for power to continue to flow to the components. 

![alt text](https://gitlab.com/krzysztof_wronski/autonomous-tree/-/raw/master/images/challenge4-controlbox.png "Initial Electronic Design")

I used the CNC to precisely mill out openings in the plastic box. The openings were needed for the key switch, 3 buttons, as well as the dot-matrix display. Because of the height of the box and a failure to properly secure it on the CNC bed, the side openings for the power connectors and symbolic ethernet ports were not cut using the CNC after breaking an end-mill bit attempting to do so. A jigsaw was used using a paper template to cut out the openings by hand and then I designed additional 3D printed frames to ensure a precise fit for the connectors and a clean look.

![alt text](https://gitlab.com/krzysztof_wronski/autonomous-tree/-/raw/master/images/challenge4-3.png "Box Interior")


The box is powered by a 12V power source because the two motors powering the radar and lidar modules, blinking light, as well as the LEDs inside the colorful switch buttons run on 12V. The ESP32 and LED display run on 5V so a downconverter is used to step down 12V  to 5V. Power is provided using jump leads from a lithium car battery starter that I already owned. The whole system, in its most consuming state, draws no more than 0,5amps and while testing the components I found that the battery I had would power all the lights, ESP32, and two motors for 8 hours or more. One insight from setting up the curcuit was that I did not need to have motors with unique RPM gearing. Instead, I could control speed and direction using the motor driver board and PWM. I also would have preferred to use buttons rather than momentary switches to actuate my 3 inputs, but the ESP32 allowed me to achieve the same result. There were some challenges with soldering and intially using low-grade jumper cables that fell apart under stress but I did my best to replace the weakest cables and protect the rest with hot glue.

![alt text](https://gitlab.com/krzysztof_wronski/autonomous-tree/-/raw/master/images/challenge4-test1.png "Power Test")

**Inventory for Control Box:**

- 1x ESP32
- 1x Downconverter (Adjusted !2V to 5V)
- 1x L298N Dual H Bridge Stepper Motor Driver Board
- 1x AptoFun Dot Matrix MAX7219 MCU
- 3x 16mm momentary switch with 12V lamp
- 1x Key operated switch
- 1x Emergency stop button
- 5x Power connectors (female) and plugs (male)
- 1x Mosfet
- 3x Resistors
- Jumper cables


## Code

The code used for this project is quite simple and provides the following functions:

- 1 button toggles on both the radar and lidar motors simultanesouly and keeps them on until the button is pressed again
- 1 button does the same for the blinking safety lamp
- Each momentary switch on the control box has a 400ms delay to create the behavior of an on/off button function
- Continously displays the current queue number in the sequence on the dot matrix display in a scrolling animation
- 1 button cycles to the next number in the queue, beginning the sequence at D105 and progressing from there one at a time
- A 1000ms delay ensures the queue number does not grow too quickly when the switch is actuated

For the MAX7219 dot matrix display, a library was used for displaying the characters and configuring the scrolling (LEDMatrix Library) and is included in the code directory.

<br>

## Mounting Structure and Signage

Finally, a wooden structure was created on the CNC utilsing wood from another student project to create circular mounting pads for the objects and a wooden frame on which they can be supported and mounted to the tree using straps or cable ties. The circular wooden pieces were cut out on the CNC using a 6mm endmill bit and the frame was cut and assembled using a shop saw, screws, and wood glue. Once the the structure was complete, all the 3D printed parts could be mounted and long electrical cable was wired to all the components to provide enough length for the objects to be separated significantly from the control box. All output device wires end with a male power plug to neatly and easily plug into the control box. Ethernet cable was used for aesthetic purposes to create the appearance that data is being transmitted to and from the ground and the tree canopy.

![alt text](https://gitlab.com/krzysztof_wronski/autonomous-tree/-/raw/master/images/challenge4-wood.gif "CNC of Wood Structure")


Finally, I used the laser and vinyl cutter to create signage and labels that would give my insallation an official public service look and quality. The laser cutter was used to etch text into acrylic and then cut out the acrylic panel to the approrpriate size to match the width of the wooden sign. Spray paint and alcohol was  used to darken the etched lettering and wipe off the excess paint. This was then attached to a wooden board with a rounded top (repurposed from another student project) and reflective tape was applied onto the wood to create the appearance of a road sign. The sign has a wooden base attached to the main panel with two L-brackets and can be attached into the ground with long bolts or screws.  

![alt text](https://gitlab.com/krzysztof_wronski/autonomous-tree/-/raw/master/images/challenge4-signs.png "Signage")

**Inventory for Mounting Stucture and Signage:**

- 18 M4 machine screws
- 30 M4 wood screws
- 7 meters of electrical cable (power + ground)
- Reflective tape
- Wooden board
- 2x L-brackets

<br>
## Testing and Setting Up

Once all the parts were complete, it was time to  to install the entire system outside and learn from how it would mount to a tree. I needed to go on a scouting mission to identify a tree with the appropriate scale and surrounding environment and understand  what tools I would need to install it. It was very important that the installation was completely uninvasive to the tree and could be put up and taken down without many people, tools, or time.

![alt text](https://gitlab.com/krzysztof_wronski/autonomous-tree/-/raw/master/images/challenge4-test2.gif "Outdoor Test")


After the trial, I realised that, for easy installation, I would need to elongate the main wooden post holding the radar, antenna, and anemometer. I simply attached a longer piece of wood to make this post easier to install over the tree canopy. That said, for the MDEFest installation, I still needed another person and a ladder to install it as desired at a high enoguh position above the tree canopy.

![alt text](https://gitlab.com/krzysztof_wronski/autonomous-tree/-/raw/master/images/challenge4-1.gif "Demo")

The resulting installation, combined with a chat-bot interface created using the Landbot SaaS tool, was very successful and received very positive feedback. A PDF of the chat-bot conversational flow and script is included in the repository.

![alt text](https://gitlab.com/krzysztof_wronski/autonomous-tree/-/raw/master/images/challenge4-chatbot.png "Chatbot Flow")

<br>

## Next Steps

Using images and video captured of the first installation and the 15 participants that came to visit the tree on June 21, 2021, I will now pitch this project for various festivals and/or exhibitions and understand the demand or thematic connections it can fit into. I would like to adapt what I already have to specific contexts and locations but also add functionality to the queueing system to potentially have people book appointments in advance and check-in using a webpage. Additionally, it would be very interesting if there was a way to make some of the sensors actually collect data that could be displayed back or captured to enhance the experience or feeling that the tree is autonomously sensing and utilising information about what is happening around them. Finally, based on feedback from the visitors, a major research area would be to uncover and explore a communication method between human and tree that is more organic rather than human-centered. 

![alt text](https://gitlab.com/krzysztof_wronski/autonomous-tree/-/raw/master/images/challenge4-4.gif "Chatbot")
<br>

## License

Attribution 4.0 International (CC BY 4.0)
This is a human-readable summary of (and not a substitute for) the license.

**You are free to:**

- Share — copy and redistribute the material in any medium or format
- Adapt — remix, transform, and build upon the material for any purpose, even commercially.
- This license is acceptable for Free Cultural Works.
- The licensor cannot revoke these freedoms as long as you follow the license terms.

**Under the following terms:**

Attribution — You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.

No additional restrictions — You may not apply legal terms or technological measures that legally restrict others from doing anything the license permits.


