(00-Screws6mmFlat.nc)
(----------------------------------------------------------------)
( Generated for RhinoCam to export for CNC-STEP models Raptor X-SL3000s20 / T-Rex1215 )
( Postprocessor written at FabLab BCN/IaaC by eduardo.chamorro@iaac.net )
( Last edit : 22 January - 2019 )
(----------------------------------------------------------------)
(Stock Size X = 1400.0000, Y =  150.0000, Z = 15.0000)
(Home Origin X =  0, Y = 0, Z = 0)
(Units = MM, Spindle Speed = 18000)
(Max cut depth = Z-15.0000)
(Tool dia= 6.0 mm, Tool length= 30.0 mm)(WARNING!-CHECK YOUR TOOL)
(-------------------------------------------------------------)
%
G90
G64
M7
M8
T1
S18000
M3
G0 Z20.0000
G0 X97.6857 Y10.0307
G1 X97.6857 Y10.0307 Z-3.0000 F2500.
G0 Z20.0000
G0 X97.6857 Y10.0307
G0 Z20.0000
G0 X99.7239 Y140.0307
G1 X99.7239 Y140.0307 Z-3.0000 F2500.
G0 Z20.0000
G0 X99.7239 Y140.0307
G0 Z20.0000
G0 X1103.2598 Y140.0307
G1 X1103.2598 Y140.0307 Z-3.0000 F2500.
G0 Z20.0000
G0 X1103.2598 Y140.0307
G0 Z20.0000
G0 X1103.2598 Y10.0307
G1 X1103.2598 Y10.0307 Z-3.0000 F2500.
G0 Z20.0000
G0 X1103.2598 Y10.0307
M9
M5
M30
(--THE END - THANK YOU FOR USING THIS AWESOME POSTPROCESSOR---)
