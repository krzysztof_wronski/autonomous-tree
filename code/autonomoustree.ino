#include <LEDMatrixDriver.hpp>

#define BTN_1 32
#define BTN_2 13
#define BTN_3 27
#define LIGHT A5

const int motor1Pin = A0;
const int motor2Pin = A1;
const int freq = 5000;
const int motorChannel0 = 0;
const int motorChannel1 = 1;
const int resolution = 8;

// DISPLAY
const uint8_t LEDMATRIX_CS_PIN = 15;
// Number of 8x8 segments you are connecting
const int LEDMATRIX_SEGMENTS = 4;
const int LEDMATRIX_WIDTH    = LEDMATRIX_SEGMENTS * 8;
// The LEDMatrixDriver class instance
LEDMatrixDriver lmd(LEDMATRIX_SEGMENTS, LEDMATRIX_CS_PIN);
// Marquee speed (lower nubmers = faster)
const int ANIM_DELAY = 30;

void setup() {
  pinMode(BTN_1, INPUT);
  pinMode(BTN_2, INPUT);
  pinMode(BTN_3, INPUT);
  pinMode(LIGHT, OUTPUT);

  ledcSetup(motorChannel0, freq, resolution);
  ledcSetup(motorChannel1, freq, resolution);
  ledcAttachPin(motor1Pin, motorChannel0);
  ledcAttachPin(motor2Pin, motorChannel1);
  
  Serial.begin(9600);
  
  lmd.setEnabled(true);
  lmd.setIntensity(2);
}

int stat = 0;
int offset = 105;
char textWelcome[] = "WELCOME";
char textCustomer[] = "# D";
char text[] = "** LED MATRIX DEMO! ** (1234567890) ++ \"ABCDEFGHIJKLMNOPQRSTUVWXYZ\" ++ <$%/=?'.@,> --";
bool btn_motors_stat = false;
bool btn_leds_stat = false;

int x=0, y=0;   // start top left

void loop() {
  Serial.print("queue:");
  Serial.println(stat);
  int s = (btn_motors_stat) ? 200 : 0;
  bool l = (btn_leds_stat) ? HIGH : LOW;
  ledcWrite(motorChannel0, s);
  ledcWrite(motorChannel1, s);
  digitalWrite(LIGHT, l);
  
  if(digitalRead(BTN_1) == LOW) { 
    btn_motors_stat = !btn_motors_stat;
    delay(400);
  }
  if(digitalRead(BTN_2) == LOW) {
    btn_leds_stat = !btn_leds_stat;
    delay(400);
  }

  if(digitalRead(BTN_3) == LOW) {
    stat++;
    delay(1000);
  }

  char myConcatenation[80];
  sprintf(myConcatenation,"%s%i",textCustomer,stat+105);
  
  int len = strlen(myConcatenation);
  drawString(myConcatenation, len, x, 0);
  lmd.display();
  
  delay(ANIM_DELAY);
  if( --x < len * -8 ) {
      x = LEDMATRIX_WIDTH;
  }
}
